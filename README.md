# GTK theme set for TRIOS.  
  
Set is comprised of three variants, Light, Grey and Dark.   
Each with red, blue, green or grey color accents.

For the moment the main target is XFCE, although no major usabillity issues were spoted on other desktops.  
  
Currently suported WM's are xfwm4, openbox & unity.  
PekWM and Metacity themes are planned/in progress.  
  
KDE color schemes might also materialize some day.

-----
## Requirements:

* Murrine and Pixmap engines for GTK2
* GTK 3.14 ( should be mostly ok with 3.16 )

-----
## Downloading and building:  

After cloning this repository, or downloading and unpacking the [archive](https://gitlab.com/Cofi/trios-gtk-themes/repository/archive.zip), go to to "trios-gtk-themes" directory, open terminal and run the builder script:  
`./trios-themes-builder all`  
It should generate themes and prompt you to install them, afterwitch they are ready to use. 

-----
## Issues/Feedback/Whishlist:

Open a report [here](https://gitlab.com/Cofi/trios-gtk-themes/issues) and, if reporting an issue, please include a screenshot of it.
